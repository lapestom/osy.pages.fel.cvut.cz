#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

int main()
{
    key_t s_key;
    union semun {
        int val;
        struct semid_ds *buf;
        ushort array[1];
    } sem_attr;
    struct sembuf asem;
    int buffer_count_sem, spool_signal_sem;

    // counting semaphore, indicating the number of available buffers.
    /* generate a key for creating semaphore  */
    if ((s_key = ftok("/tmp/free", 'a')) == -1) {
        perror("ftok");
        exit(1);
    }
    if ((buffer_count_sem = semget(s_key, 1, 0660 | IPC_CREAT)) == -1) {
        perror("semget");
        exit(1);
    }
    // giving initial values
    sem_attr.val = 10; // MAX_BUFFERS are available
    if (semctl(buffer_count_sem, 0, SETVAL, sem_attr) == -1) {
        perror(" semctl SETVAL ");
        exit(1);
    }

    // counting semaphore, indicating the number of strings to be printed.
    /* generate a key for creating semaphore  */
    if ((s_key = ftok("/tmp/data", 'a')) == -1) {
        perror("ftok");
        exit(1);
    }
    if ((spool_signal_sem = semget(s_key, 1, 0660 | IPC_CREAT)) == -1) {
        perror("semget");
        exit(1);
    }
    // giving initial values
    sem_attr.val = 0; // 0 strings are available initially.
    if (semctl(spool_signal_sem, 0, SETVAL, sem_attr) == -1) {
        perror(" semctl SETVAL ");
        exit(1);
    }
    char *shared_mem;
    int fd = shm_open("pamet", O_RDWR | O_CREAT | O_TRUNC, 0666);
    ftruncate(fd, 1000);
    shared_mem = mmap(NULL, 1000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);
    asem.sem_num = 0;
    asem.sem_flg = 0;
    for (int i = 0; i < 50; i++) {
        asem.sem_op = -1;
        if (semop(buffer_count_sem, &asem, 1) == -1) {
            perror("semop: buffer_count_sem");
            exit(1);
        }
        sprintf(shared_mem, "Data %03i\n", i);
        printf("Data %03i %p\n", i, shared_mem);
        shared_mem[9] = 0;
        shared_mem += 10;
        if (i % 10 == 9) {
            shared_mem -= 100;
        }
        asem.sem_op = 1;
        if (semop(spool_signal_sem, &asem, 1) == -1) {
            perror("semop: spool_signal_sem");
            exit(1);
        }
    }
    close(fd);
    return 0;
}
