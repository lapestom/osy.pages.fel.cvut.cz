	.section .rodata
greeting:
	.string "Hello World\n"

	.text
	.global _start
_start:
	mov $4,%eax		/* write is syscall no. 4 */
	mov $1,%ebx		/* where to write (file descriptor): 1 == stdout */
	mov $greeting,%ecx	/* address of the data */
	mov $12,%edx		/* length of the data */
	int $0x80		/* call the system */

	mov $0xfc,%eax		/* exit system call */
	xorl %ebx, %ebx		/* exit status set ebx to 0 */
	int $0x80               /* call the system */
