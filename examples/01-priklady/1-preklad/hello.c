#include <stdio.h>

int f(int x)
{
    int y;
    if (x > 0) {
        y = f(x - 1) * 2;
    } else {
        y = 10;
    }
    return y;
}

int main()
{
    int a = 1;
    int b = 2;
    int c = (a & 0xfe) + (b | 0x11);
    b = f(a);
    if (c > 1) {
        printf("a+b je %i\n", c);
    } else {
        printf("ERROR\n");
    }
    b = f(1);
    return 0;
}
