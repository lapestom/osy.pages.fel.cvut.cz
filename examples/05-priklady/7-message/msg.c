#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

struct my_msg {
    long mtype;
    int len;
    char txt[10];
};

int main()
{
    key_t s_key;
    int msg_id;
    struct my_msg msg;

    // counting semaphore, indicating the number of available buffers.
    /* generate a key for creating semaphore  */
    if ((s_key = ftok("/tmp", 'a')) == -1) {
        perror("ftok");
        exit(1);
    }
    if ((msg_id = msgget(s_key, 0660 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }
    for (int i = 0; i < 50; i++) {
        msg.mtype = 1;
        msg.len = 10;
        sprintf(msg.txt, "Data %03i\n", i);
        msg.txt[9] = 0;
        if (msgsnd(msg_id, &msg, sizeof(msg) - sizeof(long), 0) == -1) {
            perror("msgsnd");
            exit(1);
        }
    }
    if (msgrcv(msg_id, &msg, sizeof(msg) - sizeof(long), 2, 0) == -1) {
        perror("msgrcv");
        exit(1);
    }
    printf("Prijato: %s\n", msg.txt);
    if (msgctl(msg_id, IPC_RMID, 0) == -1) {
        perror("msgctl");
        exit(1);
    }
    return 0;
}
