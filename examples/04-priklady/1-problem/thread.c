#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

volatile int a;
void *fce(void *n)
{
    int i;
    for (i = 0; i < 10000; i++) {
        a += 1;
    }
    printf("a=%i\n", a);
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    pthread_t tid1, tid2;
    a = 0;
    pthread_create(&tid1, NULL, fce, NULL);
    pthread_create(&tid2, NULL, fce, NULL);
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    return 0;
}
