#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

char global[8][256];
int glob_free = 8;
int glob_end = 0;
pthread_mutex_t mutex;
pthread_cond_t empty, full;

void *producer(void *i)
{
    char str[256], *s;
    int wr_ptr = 0;

    while ((s = fgets(str, 250, stdin)) != NULL) {
        pthread_mutex_lock(&mutex);
        while (glob_free <= 0) {
            printf("Wait full\n");
            pthread_cond_wait(&full, &mutex);
        }
        memcpy(global[wr_ptr], s, 256);
        glob_free--;
        pthread_cond_signal(&empty);
        pthread_mutex_unlock(&mutex);

        wr_ptr = (wr_ptr + 1) % 8;
    }
    glob_end = 1;
    pthread_mutex_lock(&mutex);
    pthread_cond_signal(&empty);
    pthread_mutex_unlock(&mutex);
    return NULL;
}

void *consumer(void *i)
{
    int rd_ptr = 0;
    while (!glob_end) {
        pthread_mutex_lock(&mutex);
        while (glob_free >= 8 && !glob_end) {
            printf("Wait empty\n");
            pthread_cond_wait(&empty, &mutex);
        }
        if (glob_free < 8) {
            printf("Zadano: %s\n", global[rd_ptr]);
            glob_free++;
        }
        pthread_cond_signal(&full);
        pthread_mutex_unlock(&mutex);
        rd_ptr = (rd_ptr + 1) % 8;
        sleep(1);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t tid_prod, tid_cons;
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&full, NULL);
    pthread_cond_init(&empty, NULL);

    pthread_create(&tid_prod, NULL, producer, NULL);
    pthread_create(&tid_cons, NULL, consumer, NULL);

    pthread_join(tid_prod, NULL);
    pthread_join(tid_cons, NULL);

    printf("Koncim\n");
    return 0;
}
